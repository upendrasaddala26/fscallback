const {createNewDirectory, createFile, deleteFiles} = require('../problemone.js')

createNewDirectory("Directory", ()=> {
    createFile("./Directory/directoryFile1.json", '{"name": "Raju"}', () => {
        createFile("./Directory/directoryFile2.json", '{"name": "Ravi"}', () => {
            deleteFiles("./Directory/directoryFile2.json")
            deleteFiles("./Directory/directoryFile1.json")
        })
    })      
})
