
let {fs, convertingToUpper, convertingToLower, sortingTheLines,deletingFiles} = require('../problemtwo.js')

fs.readFile('../lipsum.txt', 'utf8', function(error, data) {
    if (error) {
        throw error
    } else{
        convertingToUpper(data, ()=>{
            convertingToLower(()=>{
                sortingTheLines(()=>{
                    deletingFiles()
                })
            })
        })
    }
})
