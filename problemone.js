const fs = require("fs")

function createNewDirectory(directoryName, callback){
    fs.mkdir(directoryName, function(error) {
        if (error) {
          console.log(error)
        } else {
          console.log("New directory successfully created.")
          callback()
        }
      })
}
function createFile(pathName, jsonFile, callback){
    fs.writeFile(pathName, jsonFile, function (error) {
        if (error){
            throw error
        } else{
          console.log('Saved!')
          callback(pathName)
        }
      });
}
function deleteFiles(pathName){
  fs.unlink(pathName, function (error) {
  if (error) {
    throw error
  }
  console.log('File deleted!')
  });
}

module.exports = {createNewDirectory, createFile, deleteFiles}



































                                                            







