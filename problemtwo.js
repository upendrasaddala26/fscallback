const fs = require('fs')


function appendFilename(filename){
    fs.appendFile("filenames.txt", filename, (error) =>{
        if (error){
            console.log(error);
        }
    })
}

function convertingToUpper(data, callback){
    fs.writeFile('./uppercase.txt', data.toUpperCase(), function(error){
        if (error) {
            console.log(error)
        } else{
            console.log("upper created")
            appendFilename("uppercase.txt" + "\n")
            callback()  
        }
    })
}

function convertingToLower(callback){
    try {
        const data = fs.readFileSync("./uppercase.txt", "utf-8");
        fs.writeFile('./lowercase.txt', data.toLowerCase().split(".").join("\n"), function(error){
            if (error) {
                console.log(error)
            } else{
                console.log("lower created")
                appendFilename("lowercase.txt" + "\n")
                callback()
            }
        })

      } catch (error) {
        console.log(error);
      }
}

function sortingTheLines(callback){
    try{
        const data = fs.readFileSync("./lowercase.txt", "utf8")
        fs.writeFile('./sorted.txt', data.split("\n").sort((firstWord, secondWord) => {
            if (firstWord > secondWord){
                return 1
            } else if (firstWord < secondWord){
                return -1
            } else {
                return 0
            }
        }).join("\n"), function(error) {
            if (error) {
                console.log(error)
            } else{
                console.log("sorting done")
                appendFilename("sorted.txt" +"\n")
                callback()
            }
        })
    } catch(error){
        console.log(error)
    }
}

function deletingFiles(){
    console.log("hi");
    try{
        console.log("hiii");
        const data = fs.readFileSync("./filenames.txt", 'utf8').split("\n");
        // console.log(data);
        for(let index=0;index<data.length;index++){
            if (data[index] !== ''){
                fs.unlink(`${data[index]}`, function(error){
                    if (error){
                        throw error
                    }
                })
                console.log(`${data[index]} deleted`)
            }
            return data[index]
        }
        
    } catch(error){
        console.log(error)
    }
}


module.exports = {fs,convertingToUpper, convertingToLower, sortingTheLines,deletingFiles}